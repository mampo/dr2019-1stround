#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32

def talker():
    pub = rospy.Publisher('Team1_steerAngle', Float32, queue_size=10)
    pub2 = rospy.Publisher('Team1_speed', Float32, queue_size=10)
    rospy.init_node('talker2', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    num = 0
    msg = Float32()
    msg2 = Float32()
    while not rospy.is_shutdown():
        msg.data = 75
        msg2.data = 2
        rospy.loginfo(msg.data)
        rospy.loginfo(msg2.data)
        pub.publish(msg2)
        pub2.publish(msg)
        rate.sleep

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass