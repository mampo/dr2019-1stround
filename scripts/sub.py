#!/usr/bin/env python

import rospy
from std_msgs.msg import Float32

def callback(data):
    rospy.loginfo('SteerAngle:')
    rospy.loginfo(data.data)
def callback2(data):
    rospy.loginfo('Speed:')
    rospy.loginfo(data.data)
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener2', anonymous=True)

    rospy.Subscriber('Team1_steerAngle', Float32, callback)
    rospy.Subscriber('Team1_speed', Float32, callback2)
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
