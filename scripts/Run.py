#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32

def talker():
    pub = rospy.Publisher('Team1_steerAngle', Float32, queue_size=10)
    pub2 = rospy.Publisher('Team1_speed', Float32, queue_size=10)
    rospy.init_node('talker2', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    num = 0
    msg = Float32()
    while not rospy.is_shutdown():
        msg.data = num
        rospy.loginfo(msg.data)
        pub.publish(msg)
        num = num+1
        rate.sleep

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass